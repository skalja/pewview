package cz.utb.fai.ytbwatcher.interfaces;

import cz.utb.fai.ytbwatcher.models.YoutubeDataModel;

public interface OnItemClickListener {
    void onItemClick(YoutubeDataModel item);

}
