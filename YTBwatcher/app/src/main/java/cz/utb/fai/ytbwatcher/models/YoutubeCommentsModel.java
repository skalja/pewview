package cz.utb.fai.ytbwatcher.models;

import android.os.Parcel;
import android.os.Parcelable;

public class YoutubeCommentsModel implements Parcelable {
    private String title = "";
    private String comment = "";
    private String publishedAt = "";
    private String thumbnail = "";
    private String video_id = "";

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(comment);
        dest.writeString(publishedAt);
        dest.writeString(thumbnail);
        dest.writeString(video_id);
    }

    public YoutubeCommentsModel() {
        super();
    }


    protected YoutubeCommentsModel(Parcel in) {
        this();
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        this.title = in.readString();
        this.comment = in.readString();
        this.publishedAt = in.readString();
        this.thumbnail = in.readString();
        this.video_id = in.readString();

    }

    public static final Creator<YoutubeCommentsModel> CREATOR = new Creator<YoutubeCommentsModel>() {
        @Override
        public YoutubeCommentsModel createFromParcel(Parcel in) {
            return new YoutubeCommentsModel(in);
        }

        @Override
        public YoutubeCommentsModel[] newArray(int size) {
            return new YoutubeCommentsModel[size];
        }
    };
}
