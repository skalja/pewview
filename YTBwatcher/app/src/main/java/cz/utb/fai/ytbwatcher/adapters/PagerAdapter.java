package cz.utb.fai.ytbwatcher.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import cz.utb.fai.ytbwatcher.fragments.ChannelFragment;
import cz.utb.fai.ytbwatcher.fragments.AboutFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {

    private int _NumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this._NumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch(position){
            case 0:
                ChannelFragment tab1 = new ChannelFragment();
                return tab1;
            case 1:
                AboutFragment tab2 = new AboutFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return _NumOfTabs;
    }
}
